# Serverless Python Application on GCP

## Run the application in local

````shell
pipenv install uvicorn fastapi
pipenv shell
uvicorn main:app --reload
````

Finally, the output from the console:

````shell
(serverles-python) ➜  serverles-python git:(master) ✗ uvicorn main:app --reload
INFO:     Will watch for changes in these directories: ['/Users/albertoeyo/git/apecr/cursos-udemy2/serverless-python-application-on-gcp/serverles-python']
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
INFO:     Started reloader process [11109] using statreload
INFO:     Started server process [11111]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
````

## Run the application in Docker

```shell
docker build -t serverless-python -f Dockerfile .
docker run -it -p 80:5000 --env PORT=5000 serverless-python
```
